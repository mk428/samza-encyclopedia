\documentclass[graybox,natbib,nosecnum,twocolumn]{svmult}
\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}
\usepackage{mathptmx}
\usepackage{helvet}
\usepackage{courier}
\usepackage{type1cm}
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage[bottom]{footmisc}
\usepackage[normalem]{ulem}
\usepackage{hyperref}
\usepackage{soul}
\urlstyle{rm}
\graphicspath{{figures/}}

\begin{document}
\title*{Apache Samza}
\author{Martin Kleppmann}
\institute{Martin Kleppmann \at University of Cambridge, UK. \email{martin.kleppmann@cl.cam.ac.uk}}
\maketitle

\section{Definition}

Apache Samza is an open source framework for distributed processing of high-volume event streams.
Its primary design goal is to support high throughput for a wide range of processing patterns, while providing operational robustness at the massive scale required by Internet companies.
Samza achieves this goal through a small number of carefully designed abstractions: partitioned logs for messaging, fault-tolerant local state, and cluster-based task scheduling.

\section{Overview}

Stream processing is playing an increasingly important part of the data management needs of many organizations.
Event streams can represent many kinds of data, for example, the activity of users on a website, the movement of goods or vehicles, or the writes of records to a database.

Stream processing jobs are long-running processes that continuously consume one or more event streams, invoking some application logic on every event, producing derived output streams, and potentially writing output to databases for subsequent querying.
While a batch process or a database query typically reads the state of a dataset at one point in time, and then finishes, a stream processor is never finished: it continually awaits the arrival of new events, and it only shuts down when terminated by an administrator.

Many tasks can be naturally expressed as stream processing jobs, for example:
\begin{itemize}
\item aggregating occurrences of events, e.g., counting how many times a particular item has been viewed;
\item computing the rate of certain events, e.g., for system diagnostics, reporting, and abuse prevention;
\item enriching events with information from a database, e.g., extending user click events with information about the user who performed the action;
\item joining related events, e.g., joining an event describing an email that was sent with any events describing the user clicking links in that email;
\item updating caches, materialized views, and search indexes, e.g., maintaining an external full-text search index over text in a database;
\item using machine learning systems to classify events, e.g., for spam filtering.
\end{itemize}

Apache Samza, an open source stream processing framework, can be used for any of the above applications \citep{Kleppmann:2015tz,Noghabi:2017vy}.
It was originally developed at LinkedIn, then donated to the Apache Software Foundation in 2013, and became a top-level Apache project in 2015.
Samza is now used in production at many Internet companies, including LinkedIn \citep{Paramasivam:2016nk}, Netflix \citep{Netflix:2016nx}, Uber \citep{Chen:2016bo,Hermann:2017bh}, and TripAdvisor \citep{Calisi:2016sj}.

Samza is designed for usage scenarios that require very high throughput: in some production settings, it processes millions of messages per second or trillions of events per day \citep{Feng:2015vv,Paramasivam:2016nk,Noghabi:2017vy}.
Consequently, the design of Samza prioritizes scalability and operational robustness above most other concerns.

The core of Samza consists of several fairly low-level abstractions, on top of which high-level operators have been built \citep{Pathirage:2016fr}.
However, the core abstractions have been carefully designed for operational robustness, and the scalability of Samza is directly attributable to the choice of these foundational abstractions.
The remainder of this article provides further detail on those design decisions and their practical consequences.

\section{Partitioned Log Processing}\label{sec:log-processing}

A Samza job consists of a set of Java Virtual Machine (JVM) instances, called \emph{tasks}, that each processes a subset of the input data.
The code running in each JVM comprises the Samza framework and user code that implements the required application-specific functionality.
The primary API for user code is the Java interface \texttt{StreamTask}, which defines a method \texttt{process()}.
Figure \ref{fig:samza-api} shows two examples of user classes implementing the \texttt{StreamTask} interface.

Once a Samza job is deployed and initialized, the framework calls the \texttt{process()} method once for every message in any of the input streams.
The execution of this method may have various effects, including querying or updating local state and sending messages to output streams.
This model of computation is closely analogous to a map task in the well-known MapReduce programming model \citep{Dean:2004ua}, with the difference that a Samza job's input is typically never-ending (\emph{unbounded}).

\begin{figure*}
    \includegraphics[width=\textwidth]{samza-api.eps}
    \caption{The two operators of a streaming word-frequency counter using Samza's \emph{StreamTask} API
    (Image source: \cite{Kleppmann:2015tz}, \copyright\ 2015 IEEE, reused with permission)}
    \label{fig:samza-api}
\end{figure*}

\begin{figure*}
    \includegraphics[width=\textwidth]{samza-partitions.eps}
    \caption{A Samza task consumes input from one partition, but can send output to any partition
    (Image source: \cite{Kleppmann:2015tz}, \copyright\ 2015 IEEE, reused with permission)}
    \label{fig:samza-partitions}
\end{figure*}

\begin{figure*}
    \includegraphics[width=\textwidth]{samza-state.eps}
    \caption{A task's local state is made durable by emitting a changelog of key-value pairs to Kafka
    (Image source: \cite{Kleppmann:2015tz}, \copyright\ 2015 IEEE, reused with permission)}
    \label{fig:samza-state}
\end{figure*}

Similarly to MapReduce, each Samza task is a single-threaded process that iterates over a sequence of input records.
The inputs to a Samza job are partitioned into disjoint subsets, and each input partition is assigned to exactly one processing task.
More than one partition may be assigned to the same processing task, in which case the processing of those partitions is interleaved on the task thread.
However, the number of partitions in the input determines the job's maximum degree of parallelism.

The log interface assumes that each partition of the input is a totally ordered sequence of records and that each record is associated with a monotonically increasing sequence number or identifier (known as \emph{offset}).
Since the records in each partition are read sequentially, a job can track its progress by periodically writing the offset of the last read record to durable storage.
If a stream processing task is restarted, it resumes consuming the input from the last recorded offset.

Most commonly, Samza is used in conjunction with Apache Kafka (see separate article on Kafka).
Kafka provides a partitioned, fault-tolerant log that allows publishers to append messages to a log partition, and consumers (subscribers) to sequentially read the messages in a log partition \citep{Wang:2015jd,Kreps:2011wl,Goodhope:2012wo}.
Kafka also allows stream processing jobs to reprocess previously seen records by resetting the consumer offset to an earlier position, a fact that is useful during recovery from failures.

However, Samza's stream interface is pluggable: besides Kafka, it can use any storage or messaging system as input, provided that the system can adhere to the partitioned log interface.
By default, Samza can also read files from the Hadoop Distributed Filesystem (HDFS) as input, in a way that parallels MapReduce jobs, at competitive performance \citep{Noghabi:2017vy}.
At LinkedIn, Samza is commonly deployed with \emph{Databus} inputs: Databus is a change data capture technology that records the log of writes to a database and makes this log available for applications to consume \citep{Das:2012el,Qiao:2013uv}.
Processing the stream of writes to a database enables jobs to maintain external indexes or materialized views onto data in a database and is especially relevant in conjunction with Samza's support for local state (see Section ``\nameref{sec:local-state}'').

While every partition of an input stream is assigned to one particular task of a Samza job, the output partitions are not bound to tasks.
That is, when a \texttt{StreamTask} emits output messages, it can assign them to any partition of the output stream.
This fact can be used to group related data items into the same partition: for example, in the word-counting application illustrated in Figure \ref{fig:samza-partitions}, the \texttt{SplitWords} task chooses the output partition for each word based on a hash of the word.
This ensures that when different tasks encounter occurrences of the same word, they are all written to the same output partition, from where a downstream job can read and aggregate the occurrences.

When stream tasks are composed into multistage processing pipelines, the output of one task becomes the input to another task.
Unlike many other stream processing frameworks, Samza does not implement its own message transport layer to deliver messages between stream operators.
Instead, Kafka is used for this purpose; since Kafka writes all messages to disk, it provides a large buffer between stages of the processing pipeline, limited only by the available disk space on the Kafka brokers.

Typically, Kafka is configured to retain several days or weeks worth of messages in each topic.
Thus, if one stage of a processing pipeline fails or begins to run slow, Kafka can simply buffer the input to that stage while leaving ample time for the problem to be resolved.
Unlike system designs based on backpressure, which require a producer to slow down if the consumer cannot keep up, the failure of one Samza job does not affect any upstream jobs that produce its inputs.
This fact is crucial for the robust operation of large-scale systems, since it provides fault containment: as far as possible, a fault in one part of the system does not negatively impact other parts of the system.

Messages are dropped only if the failed or slow processing stage is not repaired within the retention period of the Kafka topic.
In this case, dropping messages is desirable because it isolates the fault: the alternative~-- retaining messages indefinitely until the job is repaired~-- would lead to resource exhaustion (running out of memory or disk space), which would cause a cascading failure affecting unrelated parts of the system.

Thus, Samza's design of using Kafka's on-disk logs for message transport is a crucial factor in its scalability: in a large organization, it is often the case that an event stream produced by one team's job is consumed by one or more jobs that are administered by other teams.
The jobs may be operating at different levels of maturity: for example, a stream produced by an important production job may be consumed by several unreliable experimental jobs.
Using Kafka as a buffer between jobs ensures that adding an unreliable consumer does not negatively impact the more important jobs in the system.

Finally, an additional benefit of using Kafka for message transport is that every message stream in the system is accessible for debugging and monitoring: at any point, an additional consumer can be attached to inspect the message flow.

\section{Fault-tolerant Local State}\label{sec:local-state}

Stateless stream processing, in which any message can be processed independently from any other message, is easy to implement and scale.
However, many important applications require that stream processing tasks maintain state.
For example:
\begin{itemize}
\item when performing a join between two streams, a task must maintain an index of messages seen on each input within some time window, in order to find messages matching the join condition when they arrive;
\item when computing a rate (number of events per time interval) or aggregation (e.g., sum of a particular field), a task must maintain the current aggregate value and update it based on incoming events;
\item when processing an event requires a database query to look up some related data (e.g., looking up a user record for the user who performed the action in the event), the database can also be regarded as stream processor state.
\end{itemize}

Many stream processing frameworks use transient state that is kept in memory in the processing task, for example, in a hash table.
However, such state is lost when a task crashes or when a processing job is restarted (e.g., to deploy a new version).
To make the state fault-tolerant, some frameworks such as Apache Flink periodically write checkpoints of the in-memory state to durable storage \citep{Carbone:2015ws}; this approach is reasonable when the state is small, but it becomes expensive as the state grows \citep{Noghabi:2017vy}.

Another approach, used, for example, by Apache Storm, is to use an external database or key-value store for any processor state that needs to be fault-tolerant.
This approach carries a severe performance penalty: due to network latency, accessing a database on another node is orders of magnitude slower than accessing local in-process state \citep{Noghabi:2017vy}.
Moreover, a high-throughput stream processor can easily overwhelm the external database with queries; if the database is shared with other applications, such overload risks harming the performance of other applications to the point that they become unavailable \citep{Kreps:2014wm}.

In response to these problems, Samza pioneered an approach to managing state in a stream task that avoids the problems of both checkpointing and remote databases.
Samza's approach to providing fault-tolerant local state has subsequently been adopted in the Kafka Streams framework (see article on Apache Kafka).

Samza allows each task to maintain state on the local disk of the processing node, with an in-memory cache for frequently accessed items.
By default, Samza uses RocksDB, an embedded key-value store that is loaded into the JVM process of the stream task, but other storage engines can also be plugged in its place.
In Figure \ref{fig:samza-api}, the \texttt{CountWords} task accesses this managed state through the \texttt{KeyValueStore} interface.
For workloads with good locality, Samza's RocksDB with cache provides performance close to in-memory stores; for random-access workloads on large state, it remains significantly faster than accessing a remote database \citep{Noghabi:2017vy}.

If a job is cleanly shut down and restarted, for example, to deploy a new version, Samza's host affinity feature tries to launch each StreamTask instance on the machine that has the appropriate RocksDB store on its local disk (subject to available resources).
Thus, in most cases the state survives task restart without any further action.
However, in some cases~-- for example, if a processing node suffers a full system failure~-- the state on the local disk may be lost or rendered inaccessible.

In order to survive the loss of local disk storage, Samza again relies on Kafka.
For each store containing state of a stream task, Samza creates a Kafka topic called a \emph{changelog} that serves as a replication log for the store.
Every write to the local RocksDB store is also encoded as a message and published to this topic, as illustrated in Figure \ref{fig:samza-state}.
These writes can be performed asynchronously in batches, enabling much greater throughput than synchronous random-access requests to a remote data store.
The write queue only needs to be flushed when the offsets of input streams are written to durable storage, as described in the last section.

When a Samza task needs to recover its state after the loss of local storage, it reads all messages in the appropriate partition of the changelog topic and applies them to a new RocksDB store.
When this process completes, the result is a new copy of the store that contains the same data as the store that was lost.
Since Kafka replicates all data across multiple nodes, it is suitable for fault-tolerant durable storage of this changelog.

If a stream task repeatedly writes new values for the same key in its local storage, the changelog contains many redundant messages, since only the most recent value for a given key is required in order to restore local storage.
To remove this redundancy, Samza uses a Kafka feature called \emph{log compaction} on the changelog topic.
With log compaction enabled, Kafka runs a background process that searches for messages with the same key and discards all but the most recent of those messages.
Thus, whenever a key in the store is overwritten with a new value, the old value is eventually removed from the changelog.
However, any key that is not overwritten is retained indefinitely by Kafka.
This compaction process, which is very similar to internal processes in log-structured storage engines, ensures that the storage cost and recovery time from a changelog corresponds to the size of the state, independently of the total number of messages ever sent to the changelog \citep{Kleppmann:2017wj}.

\section{Cluster-based Task Scheduling}\label{sec:scheduling}

When a new stream processing job is started, it must be allocated computing resources: CPU cores, RAM, disk space, and network bandwidth.
Those resources may need to be adjusted from time to time as load varies and reclaimed when a job is shut down.

At large organizations, hundreds or thousands of jobs need to run concurrently.
At such scale, it is not practical to manually assign resources: task scheduling and resource allocation must be automated.
To maximize hardware utilization, many jobs and applications are deployed to a shared pool of machines, with each multi-core machine typically running a mixture of tasks from several different jobs.

This architecture requires infrastructure for managing resources and for deploying the code of processing jobs to the machines on which it is to be run.
Some frameworks, such as Storm and Flink, have built-in mechanisms for resource management and deployment.
However, frameworks that perform their own task scheduling and cluster management generally require a static assignment of computing resources~-- potentially even dedicated machines~-- before any jobs can be deployed to the cluster.
This static resource allocation leads to inefficiencies in machine utilization and limits the ability to scale on demand \citep{Kulkarni:2015hz}.

By contrast, Samza relies on existing cluster management software, which allows Samza jobs to share a pool of machines with non-Samza applications.
Samza supports two modes of distributed operation:
\begin{itemize}
\item A job can be deployed to a cluster managed by Apache Hadoop YARN \citep{Vavilapalli:2013eu}.
YARN is a general-purpose resource scheduler and cluster manager that can run stream processors, MapReduce batch jobs, data analytics engines, and various other applications on a shared cluster.
Samza jobs can be deployed to existing YARN clusters without requiring any special cluster-level configuration or resource allocation.

\item Samza also supports a \emph{stand-alone} mode in which a job's JVM instances are deployed and executed through some external process that is not under Samza's control.
In this case, the instances use Apache ZooKeeper \citep{Junqueira:2011jc} to coordinate their work, such as assigning partitions of the input streams.
\end{itemize}

The stand-alone mode allows Samza to be integrated with an organization's existing deployment and cluster management tools, or with cloud computing platforms: for example, Netflix runs Samza jobs directly as EC2 instances on Amazon Web Services (AWS), relying on the existing cloud facilities for resource allocation \citep{Paramasivam:2016nk}.
Moreover, Samza's cluster management interface is pluggable, enabling further integrations with other technologies such as Mesos \citep{Hindman:2011ux}.

With large deployments, an important concern is resource isolation, that is, ensuring that each process receives the resources it requested and that a misbehaving process cannot starve colocated processes of resources.
When running in YARN, Samza supports the Linux cgroups feature to enforce limits on the CPU and memory use of stream processing tasks.
In virtual machine environments such as EC2, resource isolation is enforced by the hypervisor.

\section{Summary}\label{sec:summary}

Apache Samza is a stream processing framework that is designed to provide high throughput and operational robustness at very large scale.
Efficient resource utilization requires a mixture of different jobs to share a multi-tenant computing infrastructure.
In such an environment, the primary challenge in providing robust operation is fault isolation, that is, ensuring that a faulty process cannot disrupt correctly running processes and that a resource-intensive process cannot starve others.

Samza isolates stream processing jobs from each other in several different ways.
By using Kafka's on-disk logs as a large buffer between producers and consumers of a stream, instead of backpressure, Samza ensures that a slow or failed consumer does not affect upstream jobs.
By providing fault-tolerant local state as a common abstraction, Samza improves performance and avoids reliance on external databases that might be overloaded by high query volume.
Finally, by integrating with YARN and other cluster managers, Samza builds upon existing resource scheduling and isolation technology that allows a cluster to be shared between many different applications without risking resource starvation.

\bibliographystyle{spbasic}
\bibliography{references}

\section{Cross-References}

Apache Flink\\
Apache Heron\\
Apache Kafka\\
Continuous Queries\\
Publish/Subscribe

\end{document}
